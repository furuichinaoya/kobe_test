$(function() {

  

     $('.slider-image').slick({
          infinite: true,
          slidesToShow: 1,
          slidesToScroll: 1,
          arrows: false,
          fade: true,
          asNavFor: '.slider-navi' //サムネイルのクラス名
     });
     $('.slider-navi').slick({
          infinite: true,
          slidesToShow: 4,
          slidesToScroll: 1,
          asNavFor: '.slider-image', //スライダー本体のクラス名
          focusOnSelect: true,
     });



    var menu = $('.side-menu'), // スライドインするメニューを指定
    menuBtn = $('.info'), // メニューボタンを指定
    body = $(document.body),
    menuWidth = menu.outerWidth();

    // メニューボタンをクリックした時の動き
    menuBtn.on('click', function(){
    // body に open クラスを付与する
    body.toggleClass('open');
        if(body.hasClass('open')){
            // open クラスが body についていたらメニューをスライドインする
            body.animate({'right' : menuWidth }, 320);
            menu.animate({'right' : 0 }, 320);
        } else {
            // open クラスが body についていなかったらスライドアウトする
            menu.animate({'right' : -menuWidth }, 320);
            body.animate({'right' : 0 }, 320);
        }
    });




});
