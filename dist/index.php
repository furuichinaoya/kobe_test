<?php
// Author: Shuji Yamagami
$path = realpath(dirname(__FILE__) . '') . "/";
include_once($path.'app_config.php');
include($path.'libs/meta.php');
?>
</head>
<body>

<!-- Header
================================================== -->
<?php include(APP_PATH.'/libs/header.php'); ?>


<div id="wrap">
<!-- Main Content
================================================== -->
	<main>
		<div class="main-images">
			<ul class="slider-image">
				<li><a href="#"><img src="<?php echo APP_URL; ?>assets/images/mv01.jpg"></a></li>
				<li><a href="#"><img src="<?php echo APP_URL; ?>assets/images/mv02.jpg"></a></li>
				<li><a href="#"><img src="<?php echo APP_URL; ?>assets/images/mv04.jpg"></a></li>
			</ul>
			<ul class="slider-navi">
				<li><a href="#"><img src="<?php echo APP_URL; ?>assets/images/mv01.jpg"></a></li>
				<li><a href="#"><img src="<?php echo APP_URL; ?>assets/images/mv02.jpg"></a></li>
				<li><a href="#"><img src="<?php echo APP_URL; ?>assets/images/mv04.jpg"></a></li>
			</ul>
		</div>
		<div class="news">
			<div class="inner">
				<div class="news-box">
					<div class="box-left">
						<h2>NEWS</h2>
					</div>
					<div class="box-right">
						<ul>
							<li><a href="#"><span>2018.01.09</span>1/10（WED）10:00～オフィシャルHP先行受付開始！！</a></li>
							<li><a href="#"><span>2017.12.28</span>12/29（FRI）12:00～オフィシャルHP先行受付開始！！</a></li>
							<li><a href="#"><span>2017.12.15</span>アーティストSWAY、ベリーグッドマン出演決定！！</a></li>
						</ul>
						<p>
							<a href="#">more</a>
						</p>
					</div>
				</div>
			</div>
		</div>
		<div class="guest">
			<div class="inner">
				<h2>GUEST<img src="<?php echo APP_URL; ?>assets/images/title01.svg"></h2>
				<div class="guest-menu">
					<ul>
						<li><a href="javascript:void(0)" data-id="1">ALL</a></li>
						<li><a href="javascript:void(0)" data-id="2">MODEL</a></li>
						<li><a href="javascript:void(0)" data-id="3">GUEST</a></li>
						<li><a href="javascript:void(0)" data-id="4">ARTIST</a></li>
					</ul>
				</div>
				<div class="guest-picture on" data-id="1">
					<ul>
						<li>
							<figure>
								<a href="#">
									<img src="<?php echo APP_URL; ?>assets/images/guest01.png">
								</a>
								<figcaption>河北麻友子</figcaption>
							</figure>
						</li>
						<li>
							<figure>
								<a href="#">
									<img src="<?php echo APP_URL; ?>assets/images/guest02.png">
								</a>
								<figcaption>石田ニコル</figcaption>
							</figure>
						</li>
						<li class="sp-last-child">
							<figure>
								<a href="#">
									<img src="<?php echo APP_URL; ?>assets/images/guest03.png">
								</a>
								<figcaption>八木アリサ</figcaption>
							</figure>
						</li>
						<li>
							<figure>
								<a href="#">
									<img src="<?php echo APP_URL; ?>assets/images/guest04.png">
								</a>
								<figcaption>松井愛莉</figcaption>
							</figure>
						</li>
						<li class="last-child">
							<figure>
								<a href="#">
									<img src="<?php echo APP_URL; ?>assets/images/guest05.png">
								</a>
								<figcaption>谷まりあ</figcaption>
							</figure>
						</li>
					</ul>
					<ul>
						<li class="sp-last-child">
							<figure>
								<a href="#">
									<img src="<?php echo APP_URL; ?>assets/images/guest06.png">
								</a>
								<figcaption>吉木千沙都</figcaption>
							</figure>
						</li>
						<li>
							<figure>
								<a href="#">
									<img src="<?php echo APP_URL; ?>assets/images/guest07.png">
								</a>
								<figcaption>池田美優</figcaption>
							</figure>
						</li>
						<li>
							<figure>
								<a href="#">
									<img src="<?php echo APP_URL; ?>assets/images/guest08.png">
								</a>
								<figcaption>Niki</figcaption>
							</figure>
						</li>
						<li class="sp-last-child">
							<figure>
								<a href="#">
									<img src="<?php echo APP_URL; ?>assets/images/guest09.png">
								</a>
								<figcaption>堀田茜</figcaption>
							</figure>
						</li>
						<li class="last-child">
							<figure>
								<a href="#">
									<img src="<?php echo APP_URL; ?>assets/images/guest10.png">
								</a>
								<figcaption>野崎萌香</figcaption>
							</figure>
						</li>
					</ul>
					<ul>
						<li>
							<figure>
								<a href="#">
									<img src="<?php echo APP_URL; ?>assets/images/guest11.png">
								</a>
								<figcaption>立花恵理</figcaption>
							</figure>
						</li>
						<li>
							<figure>
								<a href="#">
									<img src="<?php echo APP_URL; ?>assets/images/guest12.png">
								</a>
								<figcaption>瑛茉ジャスミン</figcaption>
							</figure>
						</li>
						<li>
							<figure>
								<a class="artist" href="#">
									<img src="<?php echo APP_URL; ?>assets/images/artist01.png">
								</a>
								<figcaption>SWAY</figcaption>
							</figure>
						</li>
						<li>
							<figure>
								<a class="artist" href="#">
									<img src="<?php echo APP_URL; ?>assets/images/artist02.png">
								</a>
								<figcaption>ベリーグッドマン</figcaption>
							</figure>
						</li>
						<li class="last-child">
							<figure>
								<a class="special-guest" href="#">
									<img src="<?php echo APP_URL; ?>assets/images/img01.png">
								</a>
								<figcaption>尼神インター</figcaption>
							</figure>
						</li>
					</ul>
				</div>

				<div class="guest-picture" data-id="2">
					<ul>
						<li>
							<figure>
								<a href="#">
									<img src="<?php echo APP_URL; ?>assets/images/guest01.png">
								</a>
								<figcaption>河北麻友子</figcaption>
							</figure>
						</li>
						<li>
							<figure>
								<a href="#">
									<img src="<?php echo APP_URL; ?>assets/images/guest02.png">
								</a>
								<figcaption>石田ニコル</figcaption>
							</figure>
						</li>
						<li>
							<figure>
								<a href="#">
									<img src="<?php echo APP_URL; ?>assets/images/guest03.png">
								</a>
								<figcaption>八木アリサ</figcaption>
							</figure>
						</li>
						<li>
							<figure>
								<a href="#">
									<img src="<?php echo APP_URL; ?>assets/images/guest04.png">
								</a>
								<figcaption>松井愛莉</figcaption>
							</figure>
						</li>
						<li class="last-child">
							<figure>
								<a href="#">
									<img src="<?php echo APP_URL; ?>assets/images/guest05.png">
								</a>
								<figcaption>谷まりあ</figcaption>
							</figure>
						</li>
					</ul>
					<ul>
						<li>
							<figure>
								<a href="#">
									<img src="<?php echo APP_URL; ?>assets/images/guest06.png">
								</a>
								<figcaption>吉木千沙都</figcaption>
							</figure>
						</li>
						<li>
							<figure>
								<a href="#">
									<img src="<?php echo APP_URL; ?>assets/images/guest07.png">
								</a>
								<figcaption>池田美優</figcaption>
							</figure>
						</li>
						<li>
							<figure>
								<a href="#">
									<img src="<?php echo APP_URL; ?>assets/images/guest08.png">
								</a>
								<figcaption>Niki</figcaption>
							</figure>
						</li>
						<li>
							<figure>
								<a href="#">
									<img src="<?php echo APP_URL; ?>assets/images/guest09.png">
								</a>
								<figcaption>堀田茜</figcaption>
							</figure>
						</li>
						<li class="last-child">
							<figure>
								<a href="#">
									<img src="<?php echo APP_URL; ?>assets/images/guest10.png">
								</a>
								<figcaption>野崎萌香</figcaption>
							</figure>
						</li>
					</ul>
					<ul>
						<li>
							<figure>
								<a href="#">
									<img src="<?php echo APP_URL; ?>assets/images/guest11.png">
								</a>
								<figcaption>立花恵理</figcaption>
							</figure>
						</li>
					</ul>
				</div>

				<div class="guest-picture" data-id="3">
					<ul>
						<li>
							<figure>
								<a class="special-guest" href="#">
									<img src="<?php echo APP_URL; ?>assets/images/img01.png">
								</a>
								<figcaption>尼神インター</figcaption>
							</figure>
						</li>
					</ul>
				</div>

				<div class="guest-picture" data-id="4">
					<ul>
						<li>
							<figure>
								<a class="artist" href="#">
									<img src="<?php echo APP_URL; ?>assets/images/artist01.png">
								</a>
								<figcaption>SWAY</figcaption>
							</figure>
						</li>
						<li>
							<figure>
								<a class="artist" href="#">
									<img src="<?php echo APP_URL; ?>assets/images/artist02.png">
								</a>
								<figcaption>ベリーグッドマン</figcaption>
							</figure>
						</li>
					</ul>
				</div>
				<div class="view-all">
					<a href="#">VIEW ALL</a>
				</div>
			</div>
		</div>
		<div class="brand">
			<h2>BRAND<img src="<?php echo APP_URL; ?>assets/images/title01.svg"></h2>
			<div class="brand-menu">
				<ul>
					<li>
						<a href="#"><img src="<?php echo APP_URL; ?>assets/images/list01.jpg"></a>
					</li>
					<li>
						<a href="#"><img src="<?php echo APP_URL; ?>assets/images/list02.jpg"></a>
					</li>
					<li>
						<a href="#"><img src="<?php echo APP_URL; ?>assets/images/list03.jpg"></a>
					</li>
					<li>
						<a href="#"><img src="<?php echo APP_URL; ?>assets/images/list04.jpg"></a>
					</li>
					<li>
						<a href="#"><img src="<?php echo APP_URL; ?>assets/images/list05.jpg"></a>
					</li>
					<li>
						<a href="#"><img src="<?php echo APP_URL; ?>assets/images/list06.jpg"></a>
					</li>
					<li class="last-child">
						<a href="#"><img src="<?php echo APP_URL; ?>assets/images/list07.jpg"></a>
					</li>
				</ul>
			</div>
			<div class="view-all">
				<a href="#">VIEW ALL</a>
			</div>
		</div>
		<div class="social-network">
			<div class="inner">
				<h2>SOCIAL NETWORK<span>#神戸コレクション</span></h2>
				<p>SNS(Instagram、Twitter、Facebook、Google+など)で「#神戸コレクション」</br>&「#kobecollection」のハッシュタグをつけて写真や動画を投稿してね!</p>
				<div class="ig-link"></div>
				<div class="bana">
					<ul>
						<li>
							<a href="#">
								<img src="<?php echo APP_URL; ?>assets/images/topbnr01.jpg">
							</a>
						</li>
						<li>
							<a href="#">
								<img src="<?php echo APP_URL; ?>assets/images/topbnr02.jpg">
							</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</main>
</div>

<!-- Footer
================================================== -->
<?php include(APP_PATH.'/libs/footer.php'); ?>
</body>
</html>
