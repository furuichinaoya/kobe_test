<header>
  <div class="header-top">
    <div class="date">
      <p>2018.3.3 SAT @ワールド記念ホール OPEN 11:30 - CLOSE 20:00</p>
    </div>
    <div class="ticket">
      <a href="#">
        <p>チケット購入はこちらから</p>
      </a>
    </div>
  </div>
  <div class="header-middle">
    <div class="sp-bell">
      <a href="#">
        <img src="<?php echo APP_URL; ?>assets/images/bell.svg">
        <span class="no2"></span>
      </a>
    </div>
    <div class="logo">
      <h1>
        <a href="#">
          <img src="<?php echo APP_URL; ?>assets/images/logo.svg">
        </a>
      </h1>
    </div>
    <div class="info">
      <img src="<?php echo APP_URL; ?>assets/images/menu_triger.svg">
    </div>
    <div class="side-menu">
      <div class="close-btn">
        <img src="<?php echo APP_URL; ?>assets/images/menu_close.svg">
      </div>
      <div class="infomation">
        <img src="<?php echo APP_URL; ?>assets/images/sidemenu_title.svg">
      </div>
      <div class="side-menu-list">
        <ul>
          <li><a href="#">チケット購入について</a></li>
          <li><a href="#">アクセス</a></li>
          <li><a href="#">エリアマップ</a></li>
          <li><a href="#">FAQ</a></li>
          <li><a href="#">お問い合わせ</a></li>
        </ul>
        <div class="side-menu-bana">
          <img src="<?php echo APP_URL; ?>assets/images/sidemenu_bnr01.png">
        </div>
      </div>
    </div>
    <div class="sp-menu">
      <a href="#">
        <span></span>
      </a>
    </div>
  </div>
  <div class="gNavi">
    <ul>
      <li><a href="#">HOME</a></li>
      <li><a href="#">GUEST</a></li>
      <li><a href="#">BRAND</a></li>
      <li><a href="#">TICKET/ACCESS</a></li>
      <li><a href="#">TIME SCHEDULE</a></li>
      <li><a href="#">BOOTH MAP</a></li>
      <li><a href="#">VILLAGE</a></li>
      <li><a href="#">SHOW REPORT</a></li>
      <li><a href="#">ABOUT</a></li>
    </ul>
  </div>
  <div class="sp-top">
    <p><span>2018.3.3 SAT</span>@ワールド記念ホール</br>
      OPEN 11:30 - CLOSE 20:00</p>
    <div class="ticket-butun">
      <a href="#">チケット購入はこちらから</a>
    </div>
  </div>
</header>
