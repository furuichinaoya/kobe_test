<footer>
  <div class="footer-top">
    <div class="inner">
      <div class="menu footer-content">
        <p>MENU</p>
        <ul>
          <li><a href="#">GUEST</a></li>
          <li><a href="#">BRAND</a></li>
          <li><a href="#">TICKET/ACCESS</a></li>
          <li><a href="#">TIME SCHEDULE</a></li>
        </ul>
        <ul>
          <li><a href="#">BOOTH MAP</a></li>
          <li><a href="#">WHAT'S</a></li>
          <li><a href="#">VILLAGE</a></li>
          <li><a href="#">SHOW REPORT</a></li>
        </ul>
      </div>
      <div class="information footer-content">
        <p>INFORMATION</p>
        <ul>
          <li><a href="#">FAQ</a></li>
          <li><a href="#">LINE@登録について</a></li>
          <li><a href="#">今までの神コレ</a></li>
          <li><a href="#">ENGLISH</a></li>
        </ul>
        <ul>
          <li><a href="#">プレスリリース</a></li>
          <li><a href="#">お問い合わせ</a></li>
          <li><a href="#">リクルート</a></li>
          <li><a href="#">会社概要</a></li>
          <li><a href="#">プライバシーポリシー</a></li>
        </ul>
      </div>
      <div class="link footer-content">
        <p>LINK</p>
        <ul>
          <li><a href="#">福岡アジアコレクション</a></li>
        </ul>
      </div>
      <div class="sns footer-content">
        <p>SNS</p>
        <ul>
          <li>
            <a href="#">
              <img src="<?php echo APP_URL; ?>assets/images/footer_sns01.svg">
            </a>
          </li>
          <li>
            <a href="#">
              <img class="youtube" src="<?php echo APP_URL; ?>assets/images/footer_sns02.svg">
            </a>
          </li>
          <li>
            <a href="#">
              <img src="<?php echo APP_URL; ?>assets/images/footer_sns03.svg">
            </a>
          </li>
          <li>
            <a href="#">
              <img src="<?php echo APP_URL; ?>assets/images/footer_sns04.svg">
            </a>
          </li>
          <li>
            <a href="#">
              <img src="<?php echo APP_URL; ?>assets/images/footer_sns05.svg">
            </a>
          </li>
        </ul>
      </div>
    </div>
  </div>
  <div class="footer-middle">
    <div class="inner">
      <p><span>主催:</span>MBS<span>後援:</span>観光庁、神戸ファッション協会、日本ファッション・ウィーク推進機構、神戸新聞社</p>
      <p><span>協力:</span>兵庫県、神戸市、三宮コレクション実行委員会</p>
      <p><span>制作:</span>神戸コレクション制作委員会</p>
    </div>
    <div class="sns-icon">
      <ul>
        <li class="fb"><iframe src="https://www.facebook.com/plugins/like.php?href=http%3A%2F%2Fwww.kobe-collection.com%2F&amp;width=120&amp;layout=button_count&amp;action=like&amp;show_faces=false&amp;share=false&amp;height=21" width="110" height="21" style="border:none;overflow:hidden;" scrolling="no" frameborder="0" allowtransparency="true"></iframe></li>
        <li class="ig"><iframe id="twitter-widget-0" scrolling="no" frameborder="0" allowtransparency="true" class="twitter-share-button twitter-share-button-rendered twitter-tweet-button" title="Twitter Tweet Button" src="https://platform.twitter.com/widgets/tweet_button.eaf4b750247dd4d0c4a27df474e7e934.ja.html#dnt=false&amp;id=twitter-widget-0&amp;lang=ja&amp;original_referer=http%3A%2F%2Fwww.kobe-collection.com%2F&amp;size=m&amp;text=%E7%A5%9E%E6%88%B8%E3%82%B3%E3%83%AC%E3%82%AF%E3%82%B7%E3%83%A7%E3%83%B3%20%E6%97%A5%E6%9C%AC%E6%9C%80%E5%A4%A7%E3%81%AE%E3%83%95%E3%82%A1%E3%83%83%E3%82%B7%E3%83%A7%E3%83%B3%E3%82%B7%E3%83%A7%E3%83%BC%2F%E3%82%A4%E3%83%99%E3%83%B3%E3%83%88%202018%20SPRING%2FSUMMER&amp;time=1515571539359&amp;type=share&amp;url=http%3A%2F%2Fwww.kobe-collection.com%2F" style="position: static; visibility: visible; width: 75px; height: 20px;" data-url="http://www.kobe-collection.com/"></iframe></li>
      </ul>
    </div>
  </div>
  <div class="footer-bottom">
    <p>© 2017 Mainichi Broadcasting System, Inc.</p>
  </div>
</footer>

<?php if(IE8_FLAG) : ?>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="<?php echo APP_URL; ?>asstes/js/lib/jquery1-8-3.min.js"><\/script>')</script>
<?php else : ?>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="<?php echo APP_URL; ?>asstes/js/lib/jquery1-12-4.min.js"><\/script>')</script>
<?php endif; ?>

<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBoJseNDjLub0FQJSAPOHRNB_yLFSUxyVA"></script>
<script type="text/javascript" src="<?php echo APP_URL; ?>assets/js/common.js"></script>

<?php include(APP_PATH.'libs/adv_mod.php'); // javascript for adv ?>
